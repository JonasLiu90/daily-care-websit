import React from 'react';
import {Jumbotron, Container, Row, Col, Form, Button, Image} from 'react-bootstrap';


class ContactUsChinese extends React.Component {
    render(){
        return(
            <div className="page-wrap">
                <Jumbotron className="contactus-jumbo" fluid>
                    <div className="contactus-jumbo-content">
                        <h2>
                            聯絡
                        </h2>

                        <h2 className="second">
                            我們
                        </h2>

                        <div className="contactus-border"></div>
                    
                        <p>我們在乎你</p>   
                    </div>        
                </Jumbotron>

                {/* 地址 工作时间 */}
                <Container >
                    <Row className="addresss-wrap">
                        <Col lg={5} md={{size: 4, offset: 1}} sm={10} >
                            <Image src="images/loaction-white.jpg" alt="location-white" fluid 
                                className="img"
                            />
                        </Col>

                        <Col lg={5} md={{size: 3, offset: 1}} sm={4}
                            className="address-background"
                        >
                            <p className="addresss-detail">
                                <strong className="title">地址</strong>
                                <br/>

                                <span>
                                    120 XXXXX St, New York, NY 10013
                                </span>
                            </p>

                            <p className="working-hours">
                                <strong className="title">工作時間</strong>
                                <br/>

                                <span>
                                    星期一:  9am - 5pm
                                    <br/>
                                    星期二:  9am - 5pm
                                    <br/>
                                    星期三:  9am - 5pm
                                    <br/>
                                    星期四:  9am - 5pm
                                    <br/>
                                    星期五:  9am - 5pm
                                    <br/>
                                    星期六: Off
                                    <br/>
                                    星期日: Off
                                </span>
                            </p>
                        </Col>
                    </Row>
                </Container>

                {/* 用户留言 */}
                <Container className="message-wrap">
                    <Row>
                        <Col lg={3} md={3} sm={10} >
                            <div className="left">
                                <h4 className="title">聯絡方式</h4>
                                
                                <p>  
                                    電話: XXX-XXX-XXXX
                                    <br/>
                                    電郵: aaadanyc@gmail.com
                                </p>
                            </div>
                        </Col>

                        <Col lg={7} md={10} sm={{size:10, offset:1}}>
                            <div className="right">
                                <h3>留下你的聯絡方式</h3>
                                
                                <Form>
                                    <Form.Group controlId="first-name">
                                        <Form.Label>你的名字</Form.Label>
                                        <Form.Control type="text" placeholder="名字" />
                                    </Form.Group>

                                    <Form.Group controlId="last-name">
                                        <Form.Label>你的姓氏</Form.Label>
                                        <Form.Control type="text" placeholder="姓" />
                                    </Form.Group>

                                    <Form.Group controlId="phone">
                                        <Form.Label>你的電話</Form.Label>
                                        <Form.Control type="number" placeholder="電話" />
                                    </Form.Group>

                                    <Form.Group controlId="email">
                                        <Form.Label>你的電郵</Form.Label>
                                        <Form.Control type="email" placeholder="xxx@gmail.com" />
                                    </Form.Group>
                                    
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>請寫下的你留言</Form.Label>
                                        <Form.Control as="textarea" rows={3} />
                                    </Form.Group>

                                    <Button className="btn" type="submit">
                                        提交
                                    </Button>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default ContactUsChinese;

