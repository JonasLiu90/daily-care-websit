import React from 'react';


class Footer extends React.Component {
    render(){
        return(
            <div className="footer">
                <p className="text-xs-center">
                    &copy; Copyright {new Date().getFullYear()}, Asian American Adult Daycare Association  
                </p>

                <p>Phone: XXX-XXX-XXXX</p>
                <p>Address: aaadanyc@gmail.com</p>
            </div>
        )
    }
}

export default Footer;